/*
Copyright 2021.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// VPNClientSpec defines the desired state of VPNClient
type VPNClientSpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "make" to regenerate code after modifying this file

	// Name is the name of the VPNClient user
	Name string `json:"name,omitempty"`

	// Email is the email of the VPNClient user
	Email string `json:"email,omitempty"`
}

// VPNClientStatus defines the observed state of VPNClient
type VPNClientStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status

// VPNClient is the Schema for the vpnclients API
type VPNClient struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   VPNClientSpec   `json:"spec,omitempty"`
	Status VPNClientStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// VPNClientList contains a list of VPNClient
type VPNClientList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []VPNClient `json:"items"`
}

func init() {
	SchemeBuilder.Register(&VPNClient{}, &VPNClientList{})
}
