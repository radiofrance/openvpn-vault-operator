/*
Copyright 2021.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"fmt"

	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	ctrllog "sigs.k8s.io/controller-runtime/pkg/log"

	vaultapi "github.com/hashicorp/vault/api"
	openvpnvaultv1 "gitlab.com/radiofrance/vault-openvpn-operator/api/v1"
)

// OperatorConfig stores configuration options for the operator
type OperatorConfig struct {
	VaultAddr         string
	VaultRoleID       string
	VaultSecretID     string
	ClientCertsKVPath string
}

// VPNClientReconciler reconciles a VPNClient object
type VPNClientReconciler struct {
	client.Client
	Scheme *runtime.Scheme
	*OperatorConfig
}

//+kubebuilder:rbac:groups=openvpn-vault.radiofrance.dev,resources=vpnclients,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=openvpn-vault.radiofrance.dev,resources=vpnclients/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=openvpn-vault.radiofrance.dev,resources=vpnclients/finalizers,verbs=update

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the VPNClient object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.8.3/pkg/reconcile
func (r *VPNClientReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	log := ctrllog.FromContext(ctx)

	// Fetch the VPNClient instance
	vpnClient := &openvpnvaultv1.VPNClient{}
	err := r.Get(ctx, req.NamespacedName, vpnClient)
	if err != nil {
		if errors.IsNotFound(err) {
			// Request object not found, could have been deleted after reconcile request.
			// Owned objects are automatically garbage collected. For additional cleanup logic use finalizers.
			// Return and don't requeue
			log.Info("VPNClient resource not found. Ignoring since object must be deleted")
			return ctrl.Result{}, nil
		}
		// Error reading the object - requeue the request.
		log.Error(err, "Failed to get VPNClient")
		return ctrl.Result{}, err
	}

	cfg := vaultapi.DefaultConfig()
	cfg.Address = r.VaultAddr
	vaultCli, err := vaultapi.NewClient(cfg)
	if err != nil {
		return ctrl.Result{}, fmt.Errorf("creating vault client: %w", err)
	}

	secret, err := vaultCli.Logical().Write("auth/approle/login", map[string]interface{}{
		"role_id":   r.VaultRoleID,
		"secret_id": r.VaultSecretID,
	})
	if err != nil {
		return ctrl.Result{}, fmt.Errorf("approle login : %w", err)
	}
	vaultCli.SetToken(secret.Auth.ClientToken)
	vault := vaultCli.Logical()
	clientCertSecretLit, err := vault.List(r.ClientCertsKVPath)
	if err != nil {
		return ctrl.Result{}, fmt.Errorf("failed to list vault secret at path %s login : %w", r.ClientCertsKVPath, err)
	}
	fmt.Printf("%v\n", clientCertSecretLit)

	return ctrl.Result{}, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *VPNClientReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&openvpnvaultv1.VPNClient{}).
		Complete(r)
}
